import { run } from '@cycle/run';
import { makeDOMDriver } from '@cycle/dom';
import { withState } from '@cycle/state';
import { App } from './components/app';

const main = withState(App);
// const main = withState(Grid);

run(main, {
    DOM: makeDOMDriver('#main-container')
});
