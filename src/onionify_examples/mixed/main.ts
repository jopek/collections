import { run } from '@cycle/run';
import { makeDOMDriver } from '@cycle/dom';
import { withState } from '@cycle/state';
import TodoApp from './TodoApp';

const main = withState(TodoApp);

run(main, {
    DOM: makeDOMDriver('#main-container')
});
