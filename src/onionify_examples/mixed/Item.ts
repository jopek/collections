import xs, { Stream } from 'xstream';
import { li, span, button, VNode, DOMSource } from '@cycle/dom';
import { StateSource } from '@cycle/state';
import isolate from '@cycle/isolate';
import Counter, {
    Sinks as CounterSinks,
    State as CounterState
} from './Counter';

export type State = {
    counter: CounterState;
    content: string;
};

export type Reducer = (prev?: State) => State | undefined;

export type Sources = {
    DOM: DOMSource;
    state: StateSource<State>;
};

export type Sinks = {
    DOM: Stream<VNode>;
    state: Stream<Reducer>;
};

export default function Item(sources: Sources): Sinks {
    const state$ = sources.state.stream;

    const counterComponent: Sinks = isolate(Counter, 'counter')(sources);

    const vdom$ = xs
        .combine(state$, counterComponent.DOM)
        .map(([state, counterDOM]) => {
            return li('.item', [
                span('.content', `${state.content} `),
                span('.delete', '(delete)'),
                counterDOM
            ]);
        });

    const removeReducer$ = sources.DOM.select('.delete')
        .events('click')
        .mapTo(function removeReducer(prevState: State): undefined {
            return void 0;
        });

    const reducer$ = xs.merge(removeReducer$, counterComponent.state);

    return {
        DOM: vdom$,
        state: reducer$
    };
}
