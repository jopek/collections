import xs, { Stream } from 'xstream';
import isolate from '@cycle/isolate';
import { div, span, ul, input, button, VNode, DOMSource } from '@cycle/dom';
import { StateSource, Lens, makeCollection } from '@cycle/state';
import Counter, { State as CounterState } from './Counter';
import Item, { State as ItemState } from './Item';
// import List, {State as ListState} from './List';

export type ItemStateWithKey = ItemState & { key: string };

export type State = {
    list: Array<ItemStateWithKey>;
    counter: CounterState;
};

export type Reducer = (prev?: State) => State | undefined;

export type Sources = {
    DOM: DOMSource;
    state: StateSource<State>;
};

export type Sinks = {
    DOM: Stream<VNode>;
    state: Stream<Reducer>;
};

export type Actions = {
    add$: Stream<string>;
};

function intent(domSource: DOMSource): Actions {
    return {
        add$: domSource
            .select('.input')
            .events('input')
            .map(inputEv =>
                domSource
                    .select('.add')
                    .events('click')
                    .mapTo(inputEv)
            )
            .flatten()
            .map(inputEv => (inputEv.target as HTMLInputElement).value)
    };
}

function model(actions: Actions): Stream<Reducer> {
    const initReducer$ = xs.of(function initReducer(prev?: State): State {
        if (prev) {
            return prev;
        } else {
            return { list: [], counter: { count: 0 } };
        }
    });

    const addReducer$ = actions.add$.map(
        content =>
            function addReducer(prevState: State): State {
                return {
                    ...prevState,
                    list: prevState.list.concat({
                        content: content,
                        counter: { count: prevState.counter.count },
                        key: String(Date.now())
                    })
                };
            }
    );

    return xs.merge(initReducer$, addReducer$);
}

function view(
    listVNode$: Stream<VNode>,
    counterVNode$: Stream<VNode>
): Stream<VNode> {
    return xs
        .combine(listVNode$, counterVNode$)
        .map(([ulVNode, counterVNode]) =>
            div([
                div([counterVNode]),
                span('New task:'),
                input('.input', { attrs: { type: 'text' } }),
                button('.add', 'Add'),
                ulVNode
            ])
        );
}

const List = makeCollection<ItemStateWithKey, any, any>({
    item: Item,
    itemKey: state => state.key,
    itemScope: key => key,
    collectSinks: instances => ({
        DOM: instances.pickCombine('DOM').map(itemVNodes => ul(itemVNodes)),
        state: instances.pickMerge('state')
    })
});

const listLens: Lens<State, Array<ItemStateWithKey>> = {
    get(state: State): Array<ItemStateWithKey> {
        return state.list;
    },
    set(state: State, listState: Array<ItemStateWithKey>): State {
        return {
            ...state,
            list: listState
        };
    }
};

export default function TodoApp(sources: Sources): Sinks {
    const listSinks: Sinks = isolate(List, { state: listLens })(sources);
    const counterSinks: Sinks = isolate(Counter, { state: 'counter' })(sources);
    const actions = intent(sources.DOM);
    const parentReducer$ = model(actions);
    const listReducer$ = listSinks.state;
    const counterReducer$ = counterSinks.state;
    const reducer$ = xs.merge(parentReducer$, listReducer$, counterReducer$);
    const vdom$ = view(listSinks.DOM, counterSinks.DOM);

    return {
        DOM: vdom$,
        state: reducer$
    };
}
