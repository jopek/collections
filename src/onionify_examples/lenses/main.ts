import { run } from '@cycle/run';
import { makeDOMDriver } from '@cycle/dom';
import { withState } from '@cycle/state';
import RadioApp from './RadioApp';

const main = withState(RadioApp);

run(main, {
    DOM: makeDOMDriver('#main-container')
});
