import xs, { Stream } from 'xstream';
import { label, input, VNode, DOMSource } from '@cycle/dom';
import { StateSource } from '@cycle/state';

export type State = {
    content: string;
};

export type Reducer = (prev?: State) => State | undefined;

export type Sources = {
    DOM: DOMSource;
    state: StateSource<State>;
};

export type Sinks = {
    DOM: Stream<VNode>;
    state: Stream<Reducer>;
};

export default function Edit(sources: Sources): Sinks {
    const state$ = sources.state.stream;

    const vdom$ = state$.map(state =>
        label([
            'Edit: ',
            input('.content', {
                attrs: { type: 'text' },
                props: { value: state.content }
            })
        ])
    );

    const editReducer$ = sources.DOM.select('.content')
        .events('input')
        .map(
            (ev: Event): Reducer =>
                function editReducer(prevState: State): State {
                    return { content: (ev.target as HTMLInputElement).value };
                }
        );

    return {
        DOM: vdom$,
        state: editReducer$
    };
}
