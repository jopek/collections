import xs, { Stream } from 'xstream';
import isolate from '@cycle/isolate';
import { div, ul, VNode, DOMSource } from '@cycle/dom';
import { StateSource, Lens, makeCollection } from '@cycle/state';
import { ListItem, State as ListItemState } from './listitem';

export type Reducer = (prev?: State) => State | undefined;

export type Sources = {
    DOM: DOMSource;
    state: StateSource<State>;
};

export type Sinks = {
    DOM: Stream<VNode>;
    state: Stream<Reducer>;
};

export type State = {
    listItems: Array<ListItemState>;
    currentIndex: number;
};

export function List(sources: Sources): Sinks {
    const initReducer$: Stream<Reducer> = xs.of(
        (prev: State): State => {
            return prev === undefined
                ? {
                      listItems: [],
                      currentIndex: -1
                  }
                : { ...prev };
        }
    );

    const listLens: Lens<State, Array<ListItemState>> = {
        get: (state: State) => (state as State).listItems,
        set: (state: State, listItems: Array<ListItemState>) => {
            console.log('LIST listLens SET');

            const idx = listItems.findIndex(
                (item, i) =>
                    (item.picClicked || item.highlight) &&
                    state.currentIndex !== i
            );

            const newCurrentIndex = idx === -1 ? state.currentIndex : idx;

            return {
                currentIndex: newCurrentIndex,
                listItems: listItems.map((li, i) => {
                    return { ...li, highlight: i === newCurrentIndex };
                })
            };
        }
    };

    const ListItemsComponent = makeCollection({
        item: ListItem,
        itemKey: (state: any, index: number) => state.path,
        itemScope: (key: string) => key,
        collectSinks: (instances: any) => ({
            DOM: instances
                .pickCombine('DOM')
                .map((itemVNodes: any) => ul('.list_view', itemVNodes)),
            state: instances.pickMerge('state')
        })
    });

    const listItemsComponentSinks: Sinks = isolate(ListItemsComponent, {
        state: listLens
    })(sources);

    const reducer$ = xs.merge(initReducer$, listItemsComponentSinks.state);

    return {
        DOM: listItemsComponentSinks.DOM,
        state: reducer$
    };
}
