import xs, { Stream } from 'xstream';
import { VNode, DOMSource } from '@cycle/dom';
import { Sources, Sinks, Reducer } from '../interfaces';

export interface State {
    path: string;
    currentIndex: number;
    maxIndex: number;
    back: boolean;
}

export const defaultState: State = {
    path: '',
    currentIndex: -1,
    maxIndex: -1,
    back: false
};

interface DOMIntent {
    next$: Stream<null>;
    prev$: Stream<null>;
    back$: Stream<null>;
    keyDown$: Stream<string>;
    keyUp$: Stream<string>;
}

export function Single({ DOM, state }: Sources<State>): Sinks<State> {
    return {
        DOM: view(state.stream),
        state: model(intent(DOM))
    };
}

function intent(DOM: DOMSource): DOMIntent {
    const prev$ = DOM.select('.prev')
        .events('click')
        .mapTo(null);

    const next$ = DOM.select('.next')
        .events('click')
        .mapTo(null);

    const back$ = DOM.select('.list')
        .events('click')
        .mapTo(null);

    const keyDown$ = DOM.select('body')
        .events('keydown')
        .map((ev: KeyboardEvent) => ev.key.toLowerCase());

    const keyUp$ = DOM.select('body')
        .events('keyup')
        .map((ev: KeyboardEvent) => ev.key.toLowerCase());

    return { prev$, next$, back$, keyDown$, keyUp$ };
}

function model(domi: DOMIntent): Stream<Reducer<State>> {
    const init$ = xs.of<Reducer<State>>(
        prevState => (prevState === undefined ? defaultState : prevState)
    );

    const left$ = domi.keyDown$.filter(k => k === 'arrowleft');
    const right$ = domi.keyDown$.filter(k => k === 'arrowright');

    xs.merge(left$, right$).subscribe({
        next: e => console.log(e)
    });

    const nextReducer$ = xs
        .merge(domi.next$, right$)
        .mapTo((prevState: State) => ({
            ...prevState,
            currentIndex: Math.min(
                prevState.maxIndex,
                Math.max(0, ++prevState.currentIndex)
            )
        }));

    const prevReducer$ = xs
        .merge(domi.prev$, left$)
        .mapTo((prevState: State) => ({
            ...prevState,
            currentIndex: Math.max(
                0,
                Math.min(prevState.maxIndex, --prevState.currentIndex)
            )
        }));

    const backReducer$ = domi.back$.mapTo((prevState: State) => ({
        ...prevState,
        back: true
    }));

    return xs.merge(init$, nextReducer$, prevReducer$, backReducer$);
}

function view(state$: Stream<State>): Stream<VNode> {
    return state$.map((state: State) => {
        console.log('SINGLE vdom');
        return (
            <div className="single_view">
                <pre>{JSON.stringify(state)}</pre>
                <br />
                <button className="list">list</button>
                <button className="prev">prev</button>
                <button className="next">next</button>
            </div>
        );
    });
}
