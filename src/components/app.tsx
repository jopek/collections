import xs, { Stream } from 'xstream';
import dropRepeats from 'xstream/extra/dropRepeats';
import isolate from '@cycle/isolate';
import { div, VNode, DOMSource } from '@cycle/dom';
import { StateSource, Lens } from '@cycle/state';
import { List, State as ListState } from './list';
import { Single, State as SingleState } from './single';

export type Reducer = (prev?: State) => State | undefined;

export type Sources = {
    DOM: DOMSource;
    state: StateSource<State>;
};

export type Sinks = {
    DOM: Stream<VNode>;
    state: Stream<Reducer>;
};

enum ViewMode {
    Single = 'single',
    List = 'list'
}

export type State = ListState & {
    mode: ViewMode;
};

export function App(sources: Sources): Sinks {
    const initReducer$: Stream<Reducer> = xs.of(
        (prev: State): State => {
            return {
                listItems: [
                    '20150816-DSC_4930.jpg',
                    '20150816-DSC_4968.jpg',
                    '20150816-DSC_5073.jpg',
                    '20150907-DSC_1393.jpg',
                    '20150907-DSC_1401.jpg',
                    '20150907-DSC_1415.jpg',
                    '20150909-DSC_2323.jpg',
                    '20160319-DSC_5022.jpg',
                    '20160319-DSC_5142.jpg'
                ].map(pic => ({
                    path: pic,
                    picClicked: false,
                    selected: false,
                    highlight: false
                })),
                currentIndex: 0,
                mode: ViewMode.List
            };
        }
    );

    const singleLens: Lens<State, SingleState> = {
        get: (state: State) => {
            const items = state.listItems;
            return {
                maxIndex: items.length - 1,
                currentIndex: state.currentIndex,
                path: items[state.currentIndex].path,
                back: false
            };
        },
        set: (state: State, singleState: SingleState) => {
            console.log('APP singleLens SET');
            if (state.mode !== ViewMode.Single) {
                return state;
            }
            const mode = singleState.back ? ViewMode.List : ViewMode.Single;
            return {
                ...state,
                currentIndex: singleState.currentIndex,
                listItems: state.listItems.map((item, i) => ({
                    ...item,
                    highlight: singleState.currentIndex === i
                })),
                mode
            };
        }
    };

    const listLens: Lens<State, ListState> = {
        get: (state: State) => state as ListState,
        set: (state: State, listState: ListState) => {
            console.log('APP listLens SET');
            let requestMode: ViewMode = state.mode;

            const newListItems = listState.listItems.map(item => {
                if (item.picClicked === true) {
                    requestMode = ViewMode.Single;
                }

                return {
                    ...item,
                    picClicked: false
                };
            });

            return {
                ...state,
                currentIndex: listState.currentIndex,
                listItems: newListItems,
                mode: requestMode
            };
        }
    };

    const listComponentSinks: Sinks = isolate(List, { state: listLens })(
        sources
    );
    const singleComponentSinks: Sinks = isolate(Single, { state: singleLens })(
        sources
    );

    sources.DOM.select('body')
        .events('scroll')
        .subscribe({
            next: e => console.log(JSON.stringify(e))
        });

    const vdom$ = xs
        .combine(
            singleComponentSinks.DOM,
            listComponentSinks.DOM,
            sources.state.stream
        )
        .compose(dropRepeats())
        .map(([singleVNode, listVNode, { mode }]) => {
            console.log('APP vdom');
            const content = (m: ViewMode) => {
                switch (m) {
                    case ViewMode.List:
                        return div('.gridmode', [{ ...listVNode }]);
                    case ViewMode.Single:
                        return div('.singlemode', [singleVNode, listVNode]);
                    default:
                        return div();
                }
            };
            return content(mode);
        });

    const reducer$ = xs.merge(
        initReducer$,
        listComponentSinks.state,
        singleComponentSinks.state
    );

    return {
        DOM: vdom$,
        state: reducer$
    };
}
