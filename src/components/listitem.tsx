import xs, { Stream } from 'xstream';
import { VNode, DOMSource, input } from '@cycle/dom';
import classNames from 'classnames';

import { Sources, Sinks, Reducer } from '../interfaces';

export interface State {
    path: string;
    selected: boolean;
    highlight: boolean;
    picClicked: boolean;
}
export const defaultState: State = {
    path: '',
    selected: false,
    highlight: false,
    picClicked: false
};

interface DOMIntent {
    select$: Stream<boolean>;
    picClick$: Stream<null>;
}

export function ListItem({ DOM, state }: Sources<State>): Sinks<State> {
    return {
        DOM: view(state.stream),
        state: model(intent(DOM))
    };
}

function intent(DOM: DOMSource): DOMIntent {
    const select$ = DOM.select('.select')
        .events('change')
        .map(ev => (ev.target as HTMLInputElement).checked);

    const picClick$ = DOM.select('.pic')
        .events('click')
        .mapTo(null);

    return { select$, picClick$ };
}

function model(domi: DOMIntent): Stream<Reducer<State>> {
    const init$ = xs.of<Reducer<State>>(
        prevState => (prevState === undefined ? defaultState : prevState)
    );

    const selectReducer$ = domi.select$.mapTo((prevState: State) => ({
        ...prevState,
        selected: !prevState.selected
    }));

    const picClickReducer$ = domi.picClick$.mapTo((prevState: State) => ({
        ...prevState,
        picClicked: true
    }));

    return xs.merge(init$, selectReducer$, picClickReducer$);
}

function view(state$: Stream<State>): Stream<VNode> {
    return state$.map((state: State) => {
        console.log('LISTITEM vdom');
        const { path, selected, picClicked, highlight } = state;
        const className = classNames('listitem', {
            picClicked,
            highlight,
            selected
        });
        return (
            <li className={className}>
                <pre className="pic">{JSON.stringify(state, null, 2)} </pre>
                <input type="checkbox" className="select" />
            </li>
        );
    });
}
