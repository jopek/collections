const path = require('path');

const appPath = (...names) => path.join(process.cwd(), ...names);

//This will be merged with the config from the flavor
module.exports = {
    entry: {
        main: [
            // appPath('src', 'onionify_examples', 'lenses', 'main.ts'),
            // appPath('src', 'onionify_examples', 'advanced', 'main.ts'),
            // appPath('src', 'onionify_examples', 'mixed', 'main.ts'),
            // appPath('src', 'onionify_examples', 'basic', 'main.js'),
            appPath('src', 'index.ts'),
            appPath('src', 'css', 'styles.scss')
        ]
    },
    output: {
        filename: 'bundle.[hash].js',
        path: appPath('build'),
        publicPath: '/'
    },
    devServer: {
        proxy: {
            '/api': 'http://localhost:3000'
        }
    }
};
